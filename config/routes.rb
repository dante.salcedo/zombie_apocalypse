Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "input_files#input"
  get "input_files/input", to: "input_files#input", as: "input_files_input"

  post "input_files/upload", to: "input_files#upload", as: "input_files_upload"
  get "input_files/upload", to: "input_files#upload_get", as: "input_files_upload_get"

  post "input_files/output", to: "input_files#output", as: "input_files_output"
  get "input_files/output", to: "input_files#output_get", as: "input_files_output_get"
end
