class InputFileContent < ActiveModel::Validator
  #Validates the input_file attribute in a InputFile instance
  #It validates: Amount of lines and content in the file
  #call in model:
  #validates_with InputFileValidator, {attr_name:"input_file"}
  # @param [InputFile] record.
  # @param [String] :attr_name. i.e: "input_file"
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if !attr_value.is_a?(NilClass)
      if InputFilesModule::INPUT_FILE_CLASSES.include? attr_value.class
        lines_array = validate_rows(record,attr_name,attr_value)
        if !lines_array.empty?
          validate_content(record,attr_name,lines_array)
        end
      end
    end#else case will be validate by presence
  end

  private
  #Count the lines in the file and return the lines read.
  # @param [InputFile] record The instance to validate
  # @param [String] attr_name i.e: "input_file"
  # @param [Rack::Test::UploadedFile,ActionDispatch::Http::UploadedFile] attr_value
  # @return [Array<String>] Array with lines in the file
  def validate_rows(record,attr_name,attr_value)
    lines_array = attr_value.read.split("\n")
    attr_value.close #no more readings in the validation process
    if lines_array.length != InputFilesModule::INPUT_LINES
      error_msg= I18n.t('messages.errors.input_file.wrong_lines',input_lines:InputFilesModule::INPUT_LINES)
      record.errors.add(attr_name,error_msg)
      return []
    else
      return lines_array
    end
  end

  #Validates each line in the file. Each line will be match with its respective regex
  # @param [InputFile] record The instance to validate
  # @param [String] attr_name i.e: "input_file"
  # @param [Array<String>] lines_array Files lines, each element is a line
  def validate_content(record,attr_name,lines_array)
    wrong_lines = []
    0.upto(InputFilesModule::INPUT_LINES-1) do |i|
      if !InputFilesModule::REGEXES_BY_LINE[i].match?(lines_array[i])
        wrong_lines.append(lines_array[i])
      end
    end
    if !wrong_lines.empty?
      #there is at least 1 Wrong line
      msg = I18n.t('messages.errors.input_file.wrong_content',wrong_lines:wrong_lines)
      record.errors.add(attr_name,msg)
    end
  end

end