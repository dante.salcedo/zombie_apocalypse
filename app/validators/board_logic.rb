class BoardLogic < ActiveModel::Validator
  #Validates all zombies and creatures coordinates X,Y
  #all coordinates should be in the range: [0 to board_dimension-1]
  #This validation run when all previous validations were valid
  #Call only in Board model:
  # validates_with BoardLogic, if: -> {errors.empty?}
  # @param [Board] record Board instance
  def validate(record)
    bigger_values=[]
    dim = record.dimension
    arrays = [record.zombies_to_move,record.creatures_alive,record.zombies_moved]
    arrays.each do |array|
      array.each do |c|
        x_val = c.position.x
        bigger_values.append(x_val) unless x_val < dim
        y_val = c.position.y
        bigger_values.append(y_val) unless y_val < dim
      end
    end
    if !bigger_values.empty?
      msg = I18n.t('messages.errors.coor_>_dim',dimension:dim,bigger_values_array:bigger_values)
      record.errors.add(:board_logic,msg)
    end
  end
end