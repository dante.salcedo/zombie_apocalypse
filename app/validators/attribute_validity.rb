class AttributeValidity < ActiveModel::Validator

  #Checks if one record's attr is valid doing attr_obj.valid?
  #The record's attr must be :class_type
  #Call in model:
  #validates_with AttributeValidityValidator, {attr_name:"position",class_type:Position2D}
  # @param [ActiveModel] record.
  # @param [String] :attr_name.
  # @param [Class] :class_type. i.e: Position2D,String,Integer
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if !attr_value.is_a?(NilClass)
      if attr_value.is_a?(options[:class_type]) && !attr_value.valid?
        record.errors.add(attr_name,I18n.t('messages.errors.attr_not_valid'))
        Rails.logger.debug "Invalid #{attr_value.class} Attribute: #{attr_value.errors.full_messages}"
      end
    end
    #if attr_value == (nil). it's validated with presence:true
  end
end