class AttributeClass < ActiveModel::Validator

  #Checks if one record's attr is :class_type
  #call in model:
  #validates_with AttributeClassValidator, {attr_name:"x",class_type:Numeric}
  # @param [ActiveModel] record.
  # @param [String] :attr_name.
  # @param [Array<Class>] :classes_array. i.e: [Position2D],[String,Integer]
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if !attr_value.is_a?(NilClass)
      classes_array = options[:classes_array]
      if !classes_array.include? attr_value.class
        record.errors.add(attr_name,I18n.t('messages.errors.attr_wrong_class',classes_array: classes_array))
      end
    end
    #if attr_value == (nil). it's validated with presence:true
  end
end