class ArrayAttributeValidity < ActiveModel::Validator

  #Checks every element contained in one Array attr.
  #Every Element must be objetc.valid? = true
  #Every Element must be :class_type
  #Call in model:
  #validates_with ArrayAttributeValidity, {attr_name:"zombies_to_move",class_type:Zombie}
  # @param [ActiveModel] record.
  # @param [String] :attr_name.
  # @param [Class] :class_type. i.e: Position2D,Zombie,Creature
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name) #should be an Array
    if !attr_value.is_a?(NilClass) && attr_value.is_a?(Array)
      error_added=false #Add the error only once
      attr_value.each do |object|
        if object.is_a?(options[:class_type]) && !object.valid?
          record.errors.add(attr_name,I18n.t('messages.errors.invalid_elements')) unless error_added
          error_added=true
          Rails.logger.debug "Invalid #{object.class} Element: #{object.errors.full_messages}"
        end
      end
    end
    #if attr_value == (nil). it's validated with presence:true
  end
end