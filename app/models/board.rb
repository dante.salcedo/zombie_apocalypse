class Board
  # ------------ modules ------------
  include ActiveModel::Validations
  include BoardsModule

  # ------------ validations ------------
  validates :dimension,:move_set,:zombies_to_move, presence: true
  #empty Array [] is a valid value
  validates :creatures_alive, presence: true, unless: -> {creatures_alive.is_a?(Array)}
  validates_with AttributeClass, {attr_name:"move_set", classes_array:[MOVE_SET_CLASS]}
  validates_with AttributeClass, {attr_name:"dimension", classes_array:[DIMENSION_CLASS]}
  validates :dimension, numericality: {greater_than_or_equal_to: DIMENSION_MIN_VALUE}
  validates :move_set, format: InputFilesModule::REGEXES_BY_LINE[InputFilesModule::InputFileLines::MOVE_SET]
  validates_with ArrayAttributeValidity, {attr_name:"zombies_to_move",class_type:Zombie}
  validates_with ArrayAttributeValidity, {attr_name:"creatures_alive",class_type:Creature}
  validates_with BoardLogic,  if: -> {errors.empty?}
end