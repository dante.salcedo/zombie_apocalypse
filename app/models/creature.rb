class Creature
  # ------modules-----------
  include ActiveModel::Validations
  include CreaturesModule

  # ------validations-----------
  validates :position, presence: true
  validates_with AttributeClass, {attr_name:"position", classes_array:[POSITION_CLASS]}
  validates_with AttributeValidity, {attr_name:"position", class_type:POSITION_CLASS}
end