class Zombie < Creature
  # ------modules-----------
  #it inherits Creature's modules
  include ZombiesModule

  # ------validations-----------
  #it inherits Creature's validations
  validates_with AttributeClass, {attr_name:"moved", classes_array:MOVED_CLASSES}
end