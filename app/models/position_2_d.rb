class Position2D
  # ------- modules -----------
  include ActiveModel::Validations
  include Positions2DModule

  # ------- validations -----------
  validates :x,:y, presence: true
  #validates_with validates when :x or :y are numeric string i.e  "1" ,"2"
  validates_with AttributeClass, {attr_name:"x", classes_array:[X_CLASS]}
  validates_with AttributeClass, {attr_name:"y", classes_array:[X_CLASS]}
  #validates numericality doesn't add error with numeric string i.e  "1" "2"
  validates :x,:y, numericality: {only_integer: true, greater_than_or_equal_to: MIN_VALUE}

end