class InputFile
  # ------modules-----
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks
  include InputFilesModule

  # ------validations------
  validates :input_file, presence: true
  validates_with AttributeClass, {attr_name:"input_file", classes_array:INPUT_FILE_CLASSES}
  validates_with InputFileContent, {attr_name:"input_file"}
  after_validation :set_input_lines,  if: -> {errors.empty?}

end