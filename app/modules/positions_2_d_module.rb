module Positions2DModule
  attr_accessor :x,:y #Matrix 2D coordinates. x:Horizontal y:Vertical

  MIN_VALUE = 0 #Min value for x and y. Matrix[0][0] first position.
  X_CLASS = Integer
  Y_CLASS = Integer

  # @param [Integer] x coordinate
  # @param [Integer] y coordinate
  # @return [Position2D]
  def initialize(x,y)
    @x = x
    @y = y
  end

  # Compare the instance coordinates @x,@y with the argument coordinates x,y
  # @param [Position2D] pos_to_compare
  # @return [TrueClass, FalseClass] True if @x==x and @y==y. False in otherwise
  def equal(pos_to_compare)
    return (@x == pos_to_compare.x && @y == pos_to_compare.y)
  end

end