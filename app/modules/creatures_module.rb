module CreaturesModule
  attr_accessor :position #[Position2D] object

  POSITION_CLASS = Position2D

  # @param [Position2D] pos
  # @return [Creature]
  def initialize(pos)
    @position = pos
  end

end