module InputFilesModule
  attr_accessor :input_file
  attr_accessor :file_lines #Array<String> Each element is an file's line

  class InputFileLines
    DIMENSION=0
    INIT_ZOMBIE_POS=1
    CREATURES_POS=2
    MOVE_SET=3
  end

  INPUT_FILE_CLASSES=[Rack::Test::UploadedFile,ActionDispatch::Http::UploadedFile]
  INPUT_LINES=4
  REGEXES_BY_LINE=[]
  #Dimension Int: 4
  REGEXES_BY_LINE[InputFileLines::DIMENSION]=/\A\d+\z/
  #Initial zombie position. i.e: (2,1)
  REGEXES_BY_LINE[InputFileLines::INIT_ZOMBIE_POS]=/\A\(\d+,\d+\)\z/
  #Creature's positions: i.e: (0,1)(1,2)(3,1)
  #If No creatures should be match with '-'
  REGEXES_BY_LINE[InputFileLines::CREATURES_POS]=/\A(?:\(\d+,\d+\))+$|\A-\z/
  #Move_set that zombies will do
  # REGEXES_BY_LINE[InputFileLines::MOVE_SET]=/^[UDRL]+$/
  REGEXES_BY_LINE[InputFileLines::MOVE_SET]=/\A[UDRL]+\z/

  REGEX_GET_POS = /\((.*?)\)/ #for zombie pos (l2) and creature's positions (l3)

  # @param [Rack::Test::UploadedFile,ActionDispatch::Http::UploadedFile] file
  #[Rack::Test::UploadedFile] when testing
  #[ActionDispatch::Http::UploadedFile] when file is uploaded via GUI
  # @return [InputFile]
  def initialize(file)
    @input_file = file
  end

  # Get a board from this instance
  # this.valid? should be true, hence @lines_array is initialized
  def to_board
    if (!@file_lines.is_a?(NilClass) &&  @file_lines.length==InputFilesModule::INPUT_LINES)
      #All lines already were validated with regexes
      #LINE 1: Dimension
      dim = @file_lines[InputFileLines::DIMENSION].scan(/\d/).first.to_i
      #LINE 2: init zombie pos
      zombie_pos = get_positions_from_input(@file_lines[InputFileLines::INIT_ZOMBIE_POS]).first
      #LINE 3: creatures positions
      creatures_positions = []
      if @file_lines[InputFileLines::CREATURES_POS] != '-'
        creatures_positions = get_positions_from_input(@file_lines[InputFileLines::CREATURES_POS])
      end
      #LINE 3: move_set
      move_set = @file_lines[InputFileLines::MOVE_SET]
      return Board.new(dim,zombie_pos,creatures_positions,move_set)
    else
      return nil
    end

  end

  private
  # Convert 1 string with positions in an Array<Position2D>
  # @param [String] input_positions. i.e:(12,2)(0,2)(5,1)
  # @return [Array<Position2D>]
  def get_positions_from_input(input_positions)
    positions = []
    ocurrs=input_positions.scan(REGEX_GET_POS)#i.e: "(12,2)(0,2)(5,1)" => #[["12,2"], ["0,2"], ["5,2"]]
    ocurrs.each do |o| #o => ["12,2"]
      coords = o.first.split(',') #"12,2" => ["12", "2"]
      x_cor = coords.first.to_i #12
      y_cor = coords.last.to_i #2
      positions.append(Position2D.new(x_cor,y_cor))
    end
    return positions
  end

  # Set the lines extracted from the input_file in an Array
  # Only should be called from Model file if all instance's validations are approved
  # Call: after_validation :set_input_lines,  if: -> {errors.empty?}
  def set_input_lines
    @input_file.open #input_file is opened and closed in validations.
    lines_array = @input_file.read.split("\n")
    @input_file.close #no more readings to this file
    @file_lines = lines_array
  end
end