module ZombiesModule
  #true if it was moved according to the move_set in the file_input
  attr_accessor :moved #[Boolean].

  MOVED_CLASSES=[TrueClass,FalseClass]

  # @param [Position2D] pos
  # @return [Zombie]
  def initialize(pos)
    super(pos)
    @moved = false
  end
end