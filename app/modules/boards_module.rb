module BoardsModule
  attr_accessor :dimension #Integer. Min 1.
  attr_accessor :move_set #String. Move set that zombies will do
  attr_accessor :zombies_to_move #Array<Zombie>. Zombies not moved
  attr_accessor :creatures_alive #Array<Creature>.
  attr_accessor :zombies_moved #Array<Zombie>. Zombies moved. They did all the movements
  attr_accessor :zombies_score #Integer. 1 point each time 1 zombie touch a creature alive

  MOVE_SET_CLASS=String
  DIMENSION_CLASS=Integer
  DIMENSION_MIN_VALUE=1 #1x1 board.Min Position2D(0,0).
  RIGHT="R" #+1 in X
  LEFT="L" #-1 in X
  DOWN="D" #+1 in Y
  UP="U" #-1 in Y

  # @param [Integer] dimension
  # @param [Position2D] zombie_pos
  # @param [Array<Position2D>] creatures_positions. When no creatures, send [].
  # @param [String] move_set
  # @return [Board] board
  def initialize(dimension,zombie_pos,creatures_positions,move_set)
    @dimension = dimension
    @move_set = move_set
    @zombies_moved = []
    @zombies_score = 0
    if !zombie_pos.is_a?(NilClass) && zombie_pos.is_a?(Position2D)
      init_zombies_to_move(zombie_pos)
    end
    if !creatures_positions.is_a?(NilClass) && creatures_positions.is_a?(Array)
      init_creatures_alive(creatures_positions)
    end
  end

  # It starts the simulation, moving the zombies which convert creatures in more zombies.
  # Board status (attributes) will be changed according the rules.
  # Board instance must be valid, otherwise it will rise an Exception.
  def start_simulation
    begin
      first_zombie = true
      while(!@zombies_to_move.empty?)
        zombie = @zombies_to_move.shift #get and remove the @zombies_to_move[0]. FIFO queue.
        if first_zombie #edge case, init zombie and creature start in same position
          infections = check_infection(zombie)
          @zombies_score+=infections
          first_zombie = false
        end
        @move_set.chars.each  do |direction| #1 movement (D,L,U,R) for iteration
          zombie = move_zombie(zombie,direction,@dimension)
          infections = check_infection(zombie)
          @zombies_score+=infections
        end
        zombie.moved = true
        @zombies_moved.append(zombie)
      end
    rescue #Any error is because board.valid? is false.
      raise  StandardError.new(I18n.t('messages.errors.board.invalid'))
    end
  end

  private
  # Init the zombies_to_move Array<Zombie>
  # Create the initial zombie with the param position
  # @param [Position2D] zombie_pos
  def init_zombies_to_move(zombie_pos)
    z = Zombie.new(zombie_pos)
    @zombies_to_move=[z]
  end

  # Init the creatures_alive Array<Creatures>
  # Create 1 Creature for each Position in the array param
  # @param [Array<Position2D>] creatures_positions
  def init_creatures_alive(creatures_positions)
    @creatures_alive = []
    creatures_positions.each  do  |pos2d|
      c = Creature.new(pos2d)
      @creatures_alive.append(c)
    end
  end

  # Update the zombie's coordinates.
  # Each execution will move the zombie 1 square in 1 direction: UP,DOWN,LEFT,DOWN.
  # If the zombie surpass any border it will move to the first square in the
  # opposite side of the board following the same direction.
  # @param [Zombie] zombie to move
  # @param [String] direction Check the Class constants: UP,DOWN,LEFT,DOWN
  # @param [Integer] dim Board dimension
  # @return [Zombie] Zombie with the position updated
  def move_zombie(zombie, direction, dim)
    case direction
    when RIGHT
      zombie.position.x=(zombie.position.x+1)%dim
    when LEFT
      zombie.position.x=(zombie.position.x+dim-1)%dim
    when DOWN
      zombie.position.y=(zombie.position.y+1)%dim
    when UP
      zombie.position.y=(zombie.position.y+dim-1)%dim
    else
      return nil #will raise an Exception. board.valid? is false
    end
    return zombie
  end

  # Check for new infections. One infection happen when one zombie and one
  # o more creatures share the same position x,y.
  # Creatures infected will be removed from @creatures_alive.
  # A new zombie will be inserted in @zombies_to_move, this array works as FIFO queued.
  def check_infection(zombie)
    infections=0 #infections spread in this movement
    i=0
    while i<@creatures_alive.length #@creatures_alive may change its size
      if zombie.position.equal(@creatures_alive[i].position) #Zombie and Creature in same position
        infections+=1
        infected_creature = @creatures_alive.delete_at(i)
        @zombies_to_move.append(Zombie.new(infected_creature.position))
      else
        i+=1
      end
    end
    return infections
  end
end