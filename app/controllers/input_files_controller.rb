class InputFilesController < ApplicationController
  #GET "input_files/input"
  #GET root_url
  def input
  end

  #GET input_files/upload
  # case when refresh the output view.
  def upload_get
    render :input
  end

  #POST input_files/upload
  def upload
    input_file = InputFile.new(params[:file])
    if input_file.valid?
      board = input_file.to_board
      if board.valid?
        begin
          board.start_simulation #if error go to Exception
          @file_lines = input_file.file_lines
          @zombies_score = board.zombies_score
          @zombies_moved = board.zombies_moved
          render :output
        rescue =>  e #Any error is because board.valid? is false.
          flash.now[:error] = e #raised with StandardError.new(I18n.t('messages.errors.board.invalid'))
          render :input
        end
      else
        flash.now[:error] = board.errors.full_messages
        render :input
      end
    else
      flash.now[:error] = input_file.errors.full_messages
      render :input
    end
  end

  #POST input_files/output. render in upload
  def output
    @file_lines = []
    @zombies_score = -1
    @zombies_moved = []
  end
  #GET input_files/output.
  #case: when try to head to input_files/output. output is only POST
  def output_get
    render :input
  end
end