module ZombieHelper
  EMPTY_STRING = ''.freeze

  def format_array_zombies(zombies_array)
    if zombies_array.is_a?(Array)
      output_string = EMPTY_STRING
      zombies_array.each do |z|
        output_string += "-(#{z.position.x},#{z.position.y}) "
      end
      output_string
    else
      EMPTY_STRING
    end
  end

end


