# Problem Description


After the nuclear war, a strange and deadly virus has infected the planet. Living creatures are becoming zombies that spread their zombiness by an unfriendly bite. The world consists of an ​n​ x​ n​ grid on which `zombies​` ​and `​creatures​` live.

Both of these occupy a single square on the grid and can be addressed using zero-indexed x-​ ​y coordinates. The top left corner is `​(x: 0, y: 0)`. The horizontal coordinate is represented by `​x`​, and the vertical coordinate is represented by `​y​`. Any number of zombies and creatures may occupy the same grid square.

At the beginning of the program, a single zombie awakes and begins to move around the grid. It is given an initial ​x​-​y coordinate and a `list of movements`, up, down, left and right. E.g. (​U​,​D​,​L​,​R​).

The ordered sequence of movements needs to be represented somehow as input, for example: ​DLUURR (down, left, up, up, right, right). Zombies can move through the edge of the grid, appearing on the directly opposite side. For a 10x10 grid, a zombie moving left from (0, 4) will move to (9, 4); a zombie moving down from (3, 9) will move to (3, 0).

The poor creatures in the area are the zombie’s victims. They also have an initial ​x​-​y coordinate. The `creatures` are aware of the zombie’s presence but are so frightened that they `never move`.

If a zombie moves so that it ends up on the same square as a creature, the creature is transformed into another zombie and the zombies `score one ​point​`. The zombie continues moving and infecting creatures until it has performed all its moves.

Once it has completed its movement, the first newly created zombie moves using the same sequence as the original zombie. Once it has completed its moves, the second newly created zombie moves, and so on, in order of infection with each zombie performing the same sequence of moves. Once no new zombies have been created and all the zombies have completed moving, the program ends.

`Your task is to write a program that takes input that describes the parameters:`
```
● dimensions of the area (N)
● the initial position of the zombie. Must be 1.
● a list of positions of poor creatures. From 0 to N. 
● and a list of moves zombies will make
```

`and produces an output that shows:`
```
● the number of points scored by the zombies
● and the final positions of the zombies.
```
The input and output are not limited to a particular format. You can use json, txt, a user interface, or anything else you would like.

`Example input and output using a txt file:`

| Example input                           | Example output                                                   |
|-----------------------------------------|------------------------------------------------------------------|
| 4<br>(2,1)<br>(0,1)(1,2)(3,1)<br>DLUURR | zombies’ score: 3<br>zombies’ positions: (3,0)(2,1)(1,0)(0,0) |
If no Creatures in the Input. Write - (dash) in the 3rd line.
