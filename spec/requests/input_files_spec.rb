require 'rails_helper'

RSpec.describe "/input_files",type: :request do
  context "GET" do
    it "GET / and renders a successful response" do
      get root_url
      expect(response).to be_successful
    end
    it "GET /input_files/input and renders a successful response" do
      get input_files_input_url
      expect(response).to be_successful
    end
    it "GET /input_files/upload and renders a successful response" do
      get input_files_upload_get_url
      expect(response).to be_successful
      end
    it "GET /input_files/output and renders a successful response" do
      get input_files_output_get_url
      expect(response).to be_successful
    end
  end

  context "POST" do
    it "POST input_files/upload renders a successful response" do
      post input_files_upload_url
      expect(response).to be_successful
    end
    it "POST /input_files/output and renders a successful response" do
      post input_files_output_url
      expect(response).to be_successful
    end
  end

end