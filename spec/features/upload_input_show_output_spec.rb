require 'rails_helper'

RSpec.describe "F: Uploading a input file via GUI", type: :feature do
  it "valid file uploaded, then validates output in output_view" do
    visit input_files_input_path #visit app/views/input_files/new.html.rb
    expect(page).to have_content('Upload Input File')
    attach_file('file', 'input_files/problem_example') #args(name html tag,file name)
    click_on 'Submit' #upload file, it will start the simulation

    #if everything go well show :output, otherwise it render :input
    expect(page).to have_content('Simulation Results')
    expect(page).to have_css("table", :count => 1) #input and output
    expect(page).to have_content('Example Input')
    expect(page).to have_content('Example Output')
    expect(page).to have_link("Home",:href=>root_path)
  end

  it "invalid file uploaded, then validates errors in input_view" do
    visit input_files_input_path #visit app/views/input_files/new.html.rb
    expect(page).to have_content('Upload Input File')
    attach_file('file', 'input_files/error_lines_content') #args(name html tag,file name)
    click_on 'Submit' #upload file, it will start the simulation

    #it should render :input and show Errors
    expect(page).to have_content('Upload Input File')
    expect(page).to have_content('error')
  end

  it "invalid board, valid file uploaded, then validates errors in input_view" do
    visit input_files_input_path #visit app/views/input_files/new.html.rb
    expect(page).to have_content('Upload Input File')
    attach_file('file', 'input_files/error_coords_bigger_dim') #args(name html tag,file name)
    click_on 'Submit' #upload file, it will start the simulation

    #it should render :input and show Errors
    expect(page).to have_content('Upload Input File')
    expect(page).to have_content('error')
  end
end
