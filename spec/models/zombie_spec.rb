require 'rails_helper'

RSpec.describe Zombie, type: :model do
  it_behaves_like "a creature"

  context "attribute moved validations" do
    it "validates init value of moved attr" do
      z = Zombie.new(Position2D.new(1,2))
      expect(z.moved).to eq(false)
    end

    it "validates wrong class of moved attr" do
      z = Zombie.new(Position2D.new(1,2))
      z.moved = ""
      expect(z.valid?).to eq(false)
      expect(z.errors[:moved]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:ZombiesModule::MOVED_CLASSES)])
    end
  end
end