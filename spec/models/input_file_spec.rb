require 'rails_helper'

RSpec.describe InputFile, type: :model do
  let(:input_file) { fixture_file_upload('input_files/problem_example')}
  let(:input_no_creatures) { fixture_file_upload('input_files/no_creatures')}
  let(:input_wrong_lines) { fixture_file_upload('input_files/error_wrong_lines')}
  let(:input_wrong_content) { fixture_file_upload('input_files/error_lines_content')}

  before do
    @valid_input_file = InputFile.new(input_file)
    @valid_input_no_creatures = InputFile.new(input_no_creatures)
    @invalid_nil_file = InputFile.new(nil)
    @invalid_class_file = InputFile.new("STRING NO FILE")
    @input_wrong_lines = InputFile.new(input_wrong_lines)
    @input_wrong_content = InputFile.new(input_wrong_content)
  end

  context "attribute input_file validation" do
    it "validates one valid InputFile instance" do
      expect(@valid_input_file.valid?).to eq(true)
      expect(@valid_input_file.errors).to be_empty
    end
    it "validates one valid InputFile instance with no creatures" do
      #input file with '-' in the 3th line
      expect(@valid_input_no_creatures.valid?).to eq(true)
      expect(@valid_input_no_creatures.errors).to be_empty
    end
    it "validates presence of input_file attr" do
      expect(@invalid_nil_file.valid?).to be(false)
      expect(@invalid_nil_file.errors[:input_file]).to eq([I18n.t('errors.messages.blank')])
    end
    it "validates input_file attr class" do
      expect(@invalid_class_file.valid?).to be(false)
      expect(@invalid_class_file.errors[:input_file]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:InputFilesModule::INPUT_FILE_CLASSES)])
    end
    it "validates input_file lines quantity" do
      expect(@input_wrong_lines.valid?).to be(false)
      expect(@input_wrong_lines.errors[:input_file]).to eq([I18n.t('messages.errors.input_file.wrong_lines',input_lines:InputFilesModule::INPUT_LINES)])
    end
    it "validates input_file content" do
      expect(@input_wrong_content.valid?).to be(false)
      msg_expected = I18n.t('messages.errors.input_file.wrong_content',wrong_lines:["(2d,1)","DLUURRP"])
      expect(@input_wrong_content.errors[:input_file]).to eq([msg_expected])
      #Wrong content in lines: [\"(2d,1)\", \"DLUURRP\"]
    end
  end

  context "after InputFile instance was validated " do
    it "validates the board gotten from 'problem_example' file" do
      expect(@valid_input_file.valid?).to eq(true)
      expect(@valid_input_file.file_lines.is_a?(Array)).to eq(true)
      expect(@valid_input_file.file_lines.length).to eq(InputFilesModule::INPUT_LINES)

      board = @valid_input_file.to_board
      expect(board.is_a?(Board)).to eq(true)
      expect(board.valid?).to eq(true)
      expect(board.errors).to be_empty
      expect(board.dimension).to eq(4)
      expect(board.move_set).to eq("DLUURR")
      expect(board.zombies_moved).to eq([])
      expect(board.zombies_to_move[0].position.x).to eq(2)
      expect(board.zombies_to_move[0].position.y).to eq(1)
      expect(board.creatures_alive.length).to eq(3)
    end

    it "validates the board gotten from 'no_creatures' file" do
      expect(@valid_input_no_creatures.valid?).to eq(true)
      expect(@valid_input_no_creatures.file_lines.is_a?(Array)).to eq(true)
      expect(@valid_input_no_creatures.file_lines.length).to eq(InputFilesModule::INPUT_LINES)

      board = @valid_input_no_creatures.to_board
      expect(board.is_a?(Board)).to eq(true)
      expect(board.valid?).to eq(true)
      expect(board.errors).to be_empty
      expect(board.dimension).to eq(4)
      expect(board.move_set).to eq("DLUURR")
      expect(board.zombies_moved).to eq([])
      expect(board.zombies_to_move[0].position.x).to eq(2)
      expect(board.zombies_to_move[0].position.y).to eq(1)
      expect(board.creatures_alive).to be_empty
    end

    it "validates the board gotten from the 'error_lines_content' file" do
      expect(@input_wrong_content.valid?).to eq(false)
      expect(@input_wrong_content.file_lines.is_a?(NilClass)).to eq(true)
      board = @input_wrong_content.to_board
      expect(board.is_a?(NilClass)).to eq(true)
    end
  end
end