RSpec.shared_examples "a creature" do
  context "attribute position validations" do

    it "ensures a valid #{described_class}" do
      creature = described_class.new(Position2D.new(1,2))
      expect(creature.valid?).to eq(true)
      expect(creature.errors[:position]).to eq([])
    end

    it "validates #{described_class} with position nil" do
      creature = described_class.new(nil)
      expect(creature.valid?).to eq(false)
      expect(creature.errors[:position]).to eq([I18n.t('errors.messages.blank')])
    end

    it "validates #{described_class} with position empty String" do
      creature = described_class.new("")
      expect(creature.valid?).to eq(false)
      blank_errors = [I18n.t('errors.messages.blank'),I18n.t('messages.errors.attr_wrong_class',classes_array:[CreaturesModule::POSITION_CLASS])]
      expect(creature.errors[:position]).to eq(blank_errors)
    end

    it "validates #{described_class} with wrong class position" do
      wrong_class_pos = {"x":1,"y":2}
      creature = described_class.new(wrong_class_pos)
      expect(creature.valid?).to eq(false)
      expect(creature.errors[:position]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:[CreaturesModule::POSITION_CLASS])])
    end

    it "validates #{described_class} with invalid Position2D" do
      pos = Position2D.new("x=1","x=2") #valid? False
      #Before to create Creature you should to check pos.valid?
      creature = described_class.new(pos)
      expect(creature.valid?).to eq(false)
      expect(creature.errors[:position]).to eq([I18n.t('messages.errors.attr_not_valid')])
    end
  end
end