require 'rails_helper'

RSpec.describe Board, type: :model do
  before do
    dimension = 4 #valid coordinate's range: [0-4]
    zombie_pos = Position2D.new(2,1) #x=5 out of range
    creatures_positions = [Position2D.new(0,1),Position2D.new(1,2),Position2D.new(3,1)]
    @i_f_board = Board.new(dimension,zombie_pos,creatures_positions,"DLUURR")
  end

  context "validates model attributes" do
    it "validates wrong attributess' values" do
      b = Board.new(0,Position2D.new(-1,0),["StringA",Position2D.new(-5,-2)],"UDXRL")
      expect(b.valid?).to eq(false)
      expect(b.errors[:dimension]).to eq([I18n.t('errors.messages.greater_than_or_equal_to',count:BoardsModule::DIMENSION_MIN_VALUE)])
      expect(b.errors[:move_set]).to eq([I18n.t('errors.messages.invalid')])
      expect(b.errors[:zombies_to_move]).to eq([I18n.t('messages.errors.invalid_elements')])
      expect(b.errors[:creatures_alive]).to eq([I18n.t('messages.errors.invalid_elements')])
    end

    it "validates wrong attributes' classes" do
      b = Board.new("10",3,{"x":1,"y":2},5)
      expect(b.valid?).to eq(false)
      expect(b.errors[:move_set]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:[BoardsModule::MOVE_SET_CLASS]),I18n.t('errors.messages.invalid')])
      expect(b.errors[:dimension]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:[BoardsModule::DIMENSION_CLASS])])
      #if zombie_pos Is Not Position2D then zombies_to_move=nil
      expect(b.errors[:zombies_to_move]).to eq([I18n.t('errors.messages.blank')])
      #if creatures_positions Is Not Array then creatures_alive=nil
      expect(b.errors[:creatures_alive]).to eq([I18n.t('errors.messages.blank')])
    end

    it "validates presence of attributes" do
      b = Board.new("","","","")
      expect(b.valid?).to eq(false)
      expect(b.errors[:dimension]).to eq([I18n.t('errors.messages.blank'),I18n.t('messages.errors.attr_wrong_class',classes_array:[BoardsModule::DIMENSION_CLASS]),I18n.t('errors.messages.not_a_number')])
      expect(b.errors[:move_set]).to eq([I18n.t('errors.messages.blank'),I18n.t('errors.messages.invalid')])
      expect(b.errors[:zombies_to_move]).to eq([I18n.t('errors.messages.blank')])
      expect(b.errors[:creatures_alive]).to eq([I18n.t('errors.messages.blank')])
    end

    it "validates a valid Board instance" do
      dimension = 4
      zombie_pos = Position2D.new(2,1)
      creatures_positions = [Position2D.new(0,1),Position2D.new(1,2),Position2D.new(3,1)]
      move_set = "UDLR"
      b = Board.new(dimension,zombie_pos,creatures_positions,move_set)
      expect(b.valid?).to eq(true)
      expect(b.errors).to be_empty
    end
  end

  context "validates relation between attributes" do
    it "validates coordinates and dimension" do
      #All zombies and creatures coordinates X,Y should be in the range: [0 to dimension-1]
      dimension = 5 #valid coordinate's range: [0-4]
      wrong_zombie_pos = Position2D.new(5,0) #x=5 out of range
      wrong_creature_pos = Position2D.new(2,10)#y=10 out of range
      b = Board.new(dimension,wrong_zombie_pos,[Position2D.new(1,3),wrong_creature_pos],"UDRL")
      expect(b.valid?).to eq(false)
      expect(b.errors[:dimension]).to be_empty
      expect(b.errors[:move_set]).to be_empty
      expect(b.errors[:zombies_to_move]).to be_empty
      expect(b.errors[:creatures_alive]).to be_empty
      expect(b.errors[:board_logic]).to eq([I18n.t('messages.errors.coor_>_dim',dimension:5,bigger_values_array:[5,10])])
      ##Error=>Following coordinates values are bigger or equal than dimension(5): [5, 10]
    end
  end

  context "starting simulation in the board" do
    #This Method Starts the simulation in a valid board (input), then validates the expected output
    RSpec.shared_examples "output validation" do |board_name,board,score,expected_zombies_poss,expected_creatures_poss|
      it "validates output of #{board_name}" do
        expect(board.valid?).to eq(true)
        expect(board.is_a?(Board)).to eq(true)
        board.start_simulation
        expect(board.zombies_score).to eq(score)
        expect(board.zombies_to_move).to be_empty
        (expected_zombies_poss.length).times do |i| #Always should expect at least 1.
          z = board.zombies_moved[i]
          expect(z.position.x).to eq(expected_zombies_poss[i].x)
          expect(z.position.y).to eq(expected_zombies_poss[i].y)
        end
        if !expected_creatures_poss.empty?
          (expected_creatures_poss.length).times do |i|
            c = board.creatures_alive[i]
            expect(c.position.x).to eq(expected_creatures_poss[i].x)
            expect(c.position.y).to eq(expected_creatures_poss[i].y)
          end
        else
          expect(board.creatures_alive).to be_empty
        end
      end
    end

    #validates output of input_file
    dimension = 4 #valid coordinate's range: [0-4]
    zombie_pos = Position2D.new(2,1)
    creatures_positions = [Position2D.new(0,1),Position2D.new(1,2),Position2D.new(3,1)]
    board=Board.new(dimension,zombie_pos,creatures_positions,"DLUURR")
    expected_zombies_poss = [Position2D.new(3,0),Position2D.new(2,1),Position2D.new(1,0),Position2D.new(0,0)]
    expected_creatures_poss = []
    include_examples "output validation", "problem_example",board,3,expected_zombies_poss,expected_creatures_poss

    #Edge case when 1 zombie and 1 creature start in the same position
    dimension = 3
    zombie_pos = Position2D.new(0,0)
    creatures_positions = [Position2D.new(0,0)]
    board=Board.new(dimension,zombie_pos,creatures_positions,"LUU")
    expected_zombies_poss = [Position2D.new(2,1),Position2D.new(2,1)]
    expected_creatures_poss = []
    score = 1
    include_examples "output validation", "board with zombie init and creature in same position",board,score,expected_zombies_poss,expected_creatures_poss

    #validates output of board with no creatures
    dimension = 4
    zombie_pos = Position2D.new(1,1)
    creatures_positions = [] #3rd line in input file "-"
    board=Board.new(dimension,zombie_pos,creatures_positions,"RDRDLDRDLL")
    expected_zombies_poss = [Position2D.new(1,1)]
    expected_creatures_poss = []
    include_examples "output validation", "board with no creatures",board,0,expected_zombies_poss,expected_creatures_poss

    #validates output of board with no infections
    dimension = 3
    zombie_pos = Position2D.new(0,1)
    creatures_positions = [Position2D.new(0,2),Position2D.new(2,0),Position2D.new(1,1)]
    board=Board.new(dimension,zombie_pos,creatures_positions,"URURU")
    expected_zombies_poss = [Position2D.new(2,1)]
    expected_creatures_poss = [Position2D.new(0,2),Position2D.new(2,0),Position2D.new(1,1)]
    include_examples "output validation", "board with no infections",board,0,expected_zombies_poss,expected_creatures_poss

    #validates multiple infections in 1 movement
    dimension = 3
    zombie_pos = Position2D.new(0,0)
    #3 creatures in same position
    creatures_positions = [Position2D.new(1,1),Position2D.new(1,1),Position2D.new(1,1)]
    board=Board.new(dimension,zombie_pos,creatures_positions,"DRDR")
    #3 creatures were infected in 1 movement, which finish in the same pos
    expected_zombies_poss = [Position2D.new(2,2),Position2D.new(0,0),Position2D.new(0,0),Position2D.new(0,0)]
    expected_creatures_poss = []
    zombies_score = 3
    include_examples "output validation", "board with multiple infections in 1 move",board,zombies_score,expected_zombies_poss,expected_creatures_poss

    it "validates Exception using an invalid board" do
      expect(@i_f_board.valid?).to eq(true)
      @i_f_board.move_set="DLUXXXURR"
      expect { @i_f_board.start_simulation}.to raise_error(I18n.t('messages.errors.board.invalid'))
    end
  end
end