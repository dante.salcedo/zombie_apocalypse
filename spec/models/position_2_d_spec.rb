require 'rails_helper'

RSpec.describe Position2D, type: :model do
  before do
    @valid_position2D = Position2D.new(3,5)
    @invalid_pos2D_empty = Position2D.new("","")
    @invalid_pos2D_no_y = Position2D.new(2,nil)
    @invalid_pos2D_no_x = Position2D.new(nil,2)
    @invalid_pos2D_string_num = Position2D.new("1","2")
    @invalid_pos2D_string_char = Position2D.new("A","B")
    @invalid_pos2D_no_int = Position2D.new(3.2,4.3)
    @invalid_pos2D_neg = Position2D.new(-1,-5)
  end

  context "validates attributes" do
    it "validates a valid Position2D object" do
      expect(@valid_position2D.valid?).to eq(true)
    end

    it "validates wrong values of x and y" do
      expect(@invalid_pos2D_empty.valid?).to eq(false)
      blank_errors = [I18n.t('errors.messages.blank'),I18n.t('messages.errors.attr_wrong_class',classes_array:[Positions2DModule::X_CLASS]),I18n.t('errors.messages.not_a_number')]
      expect(@invalid_pos2D_empty.errors[:x]).to eq(blank_errors)
      expect(@invalid_pos2D_empty.errors[:y]).to eq(blank_errors)
      expect(@invalid_pos2D_no_y.valid?).to eq(false)
      expect(@invalid_pos2D_empty.errors[:y]).to eq(blank_errors)
      expect(@invalid_pos2D_no_x.valid?).to eq(false)
      expect(@invalid_pos2D_empty.errors[:x]).to eq(blank_errors)
      expect(@invalid_pos2D_string_num.valid?).to eq(false)
      expect(@invalid_pos2D_string_num.errors[:x]).to eq([I18n.t('messages.errors.attr_wrong_class',classes_array:[Positions2DModule::X_CLASS])])
      numeric_errors = [I18n.t('messages.errors.attr_wrong_class',classes_array:[Positions2DModule::X_CLASS]),I18n.t('errors.messages.not_a_number')]
      expect(@invalid_pos2D_string_char.valid?).to eq(false)
      expect(@invalid_pos2D_string_char.errors[:x]).to eq(numeric_errors)
      numeric_errors = [I18n.t('messages.errors.attr_wrong_class',classes_array:[Positions2DModule::X_CLASS]),I18n.t('errors.messages.not_an_integer')]
      expect(@invalid_pos2D_no_int.valid?).to eq(false)
      expect(@invalid_pos2D_no_int.errors[:x]).to eq(numeric_errors)
      expect(@invalid_pos2D_neg.valid?).to eq(false)
      expect(@invalid_pos2D_neg.errors[:x]).to eq([I18n.t('errors.messages.greater_than_or_equal_to',count:Positions2DModule::MIN_VALUE)])
    end
  end
end