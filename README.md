# README

* Online production version: [Click Here!](http://ec2-13-210-66-57.ap-southeast-2.compute.amazonaws.com:1111)

* Description
```
-Head to: problem_description.md
```

* Ruby version
```
Ruby 2.7.1
Rails >= 6.0.3
```

* System dependencies
```
yarn >= 1.22.10
```

* Configuration
```
$cd zombie_apocalypse/
$bundle install
$yarn install
$export RAILS_ENV=development
```

* Database creation
```
No DB. No ActiveRecord models.
Models are POROs.
```
* Run
```
$cd zombie_apocalypse/
$rails s
head to localhost:3000 in a web browser 
```

* How to run the test suite
```
$cd zombie_apocalypse/
$rspec -fdoc

F: Uploading a input file via GUI
  valid file uploaded, then validates output in output_view
  invalid file uploaded, then validates errors in input_view
  invalid board, valid file uploaded, then validates errors in input_view

Board
  validates model attributes
    validates wrong attributess' values
    validates wrong attributes' classes
    validates presence of attributes
    validates a valid Board instance
  validates relation between attributes
    validates coordinates and dimension
  starting simulation in the board
    validates output of problem_example
    validates output of board with zombie init and creature in same position
    validates output of board with no creatures
    validates output of board with no infections
    validates output of board with multiple infections in 1 move
    validates Exception using an invalid board

Creature
  behaves like a creature
    attribute position validations
      ensures a valid Creature
      validates Creature with position nil
      validates Creature with position empty String
      validates Creature with wrong class position
      validates Creature with invalid Position2D

InputFile
  attribute input_file validation
    validates one valid InputFile instance
    validates one valid InputFile instance with no creatures
    validates presence of input_file attr
    validates input_file attr class
    validates input_file lines quantity
    validates input_file content
  after InputFile instance was validated
    validates the board gotten from 'problem_example' file
    validates the board gotten from 'no_creatures' file
    validates the board gotten from the 'error_lines_content' file

Position2D
  validates attributes
    validates a valid Position2D object
    validates wrong values of x and y

Zombie
  behaves like a creature
    attribute position validations
      ensures a valid Zombie
      validates Zombie with position nil
      validates Zombie with position empty String
      validates Zombie with wrong class position
      validates Zombie with invalid Position2D
  attribute moved validations
    validates init value of moved attr
    validates wrong class of moved attr

/input_files
  GET
    GET / and renders a successful response
    GET /input_files/input and renders a successful response
    GET /input_files/upload and renders a successful response
    GET /input_files/output and renders a successful response
  POST
    POST input_files/upload renders a successful response
    POST /input_files/output and renders a successful response

Finished in 0.46058 seconds (files took 1.71 seconds to load)
43 examples, 0 failures
```